var settings = {
    plugins: {
        remove_button:{
            title:'Remove this item',
        }
    },
    onItemAdd:function() {
        this.setTextboxValue('');
        this.refreshOptions();
    }
};

document.querySelectorAll('select.tomselect').forEach((input) => {
    control = new TomSelect(input, settings);
    control.removeOption('_none');
});
